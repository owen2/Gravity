module Obstruction where

import qualified Graphics.Gloss as Gloss

type Obstruction = [Gloss.Point]

