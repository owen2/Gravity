module Grid where

import qualified Graphics.Gloss as Gloss

newtype Grid = Grid [Line]

type Line = [Gloss.Point]
type Vector = Gloss.Point

startx = -450
dpos   = 50
unit   = 100
starty = -450

defaultGrid :: Grid
defaultGrid = Grid $ map (startLine (0, dpos) (0, 900)) topPointsX ++ map (startLine (dpos, 0) (900, 0)) topPointsY where
        topPointsX = zip [startx, startx+unit .. -startx] [-450, -450..]
        topPointsY = zip [-450, -450..] [starty, starty+unit .. -starty]
        startLine (dx, dy) (xf, yf) (x, y) = zip [x, x+dx, xf+x] [y, y+dy, yf+y]

renderGrid :: (Gloss.Point -> Gloss.Vector) -> Grid -> Gloss.Picture
renderGrid f grid = Gloss.pictures (map Gloss.Line paths) where
        Grid paths = apply (simulate 1 f) grid
        simulate 0 f point = point
        simulate n f p     = simulate (n-1) f (f p)

apply :: (Gloss.Point -> Gloss.Point) -> Grid -> Grid
apply f (Grid lines) = Grid $ [map f pts | pts <- lines]
