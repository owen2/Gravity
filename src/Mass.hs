{-# LANGUAGE ExistentialQuantification #-}
module Mass where

import qualified Graphics.Gloss.Interface.Pure.Game as Gloss
import qualified Graphics.Gloss as Gloss
import Obstruction
import Data.List
import Debug.Trace

type Line = [Gloss.Point]
type Vector = Gloss.Vector

class Mass a where
        renderMass :: a -> Gloss.Picture
        updateMass :: a -> a
        totalForce :: a -> [Obstruction] -> Gloss.Point -> Vector
        respond    :: a -> Gloss.Event -> [a]

data MassObject = forall x . (Mass x) => MassObject { self :: x }

instance Mass MassObject where
        renderMass MassObject {self = self} = renderMass self
        updateMass MassObject {self = self} = MassObject {self = updateMass self}
        totalForce MassObject {self = self} = totalForce self
        respond  MassObject {self = self} e = map (\self -> MassObject {self = self}) $ respond self e

data Polygon = Polygon { vertices :: [Gloss.Point] }
                         --shatter  :: Polygon -> [Triangle] }

instance Mass Polygon where
        renderMass gon = (Gloss.Line $ vertices gon ++ (if null (vertices gon) then [] else [head (vertices gon)]))
                         `mappend` Gloss.Color (Gloss.makeColor 0.2 0.5 0.2 0.3) (Gloss.polygon (vertices gon))
        updateMass self = self
        totalForce self obstructions pt = sum $ map (\tri -> totalForce tri obstructions pt) $ shatterPoly self
        respond self event
                | Gloss.EventKey (Gloss.Char 'p') Gloss.Down _ _ <- event = map (\(Triangle (a, b, c)) -> Polygon {vertices = [a, b, c]}) $ shatterPoly _self
                | otherwise = [self] where
                        _self = trace ("vert self: " ++ (show $ vertices self)) self

--maybe don't need this?
newPoly :: [Gloss.Point] -> MassObject
newPoly pts = MassObject { self = Polygon {vertices = pts} }

newtype Triangle = Triangle (Gloss.Point, Gloss.Point, Gloss.Point)

instance Mass Triangle where
        renderMass (Triangle (a, b, c)) = renderMass $ newPoly [a, b, c]
        updateMass = id
        totalForce (Triangle (a, b, c)) _ pt = (1/3) * (a + b + c)

shatterPoly :: Polygon -> [Triangle]
shatterPoly self
        | length verts < 3 = []
        | otherwise = getTriangleFromPoint top : (shatterPoly (filterPolygonBy ((/=) top))) where
        verts = trace ("verts:" ++ (show $ vertices self)) vertices self
        l0 = length verts
        len = trace ("len " ++ show l0) $ length $ verts
        getTriangleFromPoint pt = triangleFromIndex index where
                Just index = findIndex ((==) pt) verts
        triangleFromIndex index = Triangle (verts !! ((index - 1) `mod` len),
                                      verts !! index,
                                      verts !! ((index+1) `mod` len)) where
        filterPolygonBy f = self { vertices = filter f verts }
        top = (filter (validTriangle . getIndex) $ sortBy (\(a, _) (b, _) -> a `compare` b) verts) !! 0 where
                getIndex pt = map (\(Just x) -> x) [findIndex ((==) pt) verts]
        validTriangle t = triangleIsEmpty t && triangleIsInside t
        triangleIsEmpty t = True
        triangleIsInside t = True

--pointInTriangle :: Triangle -> Gloss.Point -> Bool

--shatterPoly :: Polygon -> [Triangle]
--shatterPoly self = getTriangleFromPoint top : (shatterPoly (excludeFromPolygonBy ((==) top) self)) where
--        len = length $ vertices self
--        getTriangleFromPoint pt = newTriangle ((vertices self) !! ((index - 1) `mod` len), (vertices self) !! index, (vertices self) !! ((index  + 1) `mod` len)) where
--                Just index = findIndex ((==) pt) (vertices self)
--        excludeFromPolygonBy f self = self { vertices = filter f (vertices self)}
--        top = (filter triangleContainsNone $ sortBy (\(a, _) (b, _) -> a `compare` b) (vertices self)) !! 0
--        triangleContainsNone _ = True

--data Triangle = Triangle (Gloss.Point, Gloss.Point, Gloss.Point)
--type Triangle = Mass
--newTriangle (a, b, c) = newPoly [a, b, c]
--
--data Polygon = Polygon { vertices :: [Gloss.Point],
--        shatter :: Polygon -> [Triangle] }
--
--
--shatterPoly self = getTriangleFromPoint top : (shatterPoly (excludeFromPolygonBy ((==) top) self)) where
--        len = length $ vertices self
--        getTriangleFromPoint pt = newTriangle ((vertices self) !! ((index - 1) `mod` len), (vertices self) !! index, (vertices self) !! ((index  + 1) `mod` len)) where
--                Just index = findIndex ((==) pt) (vertices self)
--        excludeFromPolygonBy f self = self { vertices = filter f (vertices self)}
--        top = (filter triangleContainsNone $ sortBy (\(a, _) (b, _) -> a `compare` b) (vertices self)) !! 0
--        triangleContainsNone _ = True
--
--newPoly :: [Gloss.Point] -> Mass
--newPoly pts = polygon $ Polygon { vertices = pts,
--        shatter = \self -> shatterPoly self
--                }
--
--polygon :: Polygon -> Mass
--polygon gon = Mass {
--        rho = 1,
--        render = (Gloss.Line $ vertices gon ++ (if null (vertices gon) then [] else [head (vertices gon)]))
--                `mappend` Gloss.Color (Gloss.makeColor 0.2 0.5 0.2 0.3) (Gloss.polygon (vertices gon)),
--        update = (shatterPoly gon) !! 0,
--        totalForce = \_ (x, y) -> (0, 0) }
--
