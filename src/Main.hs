module Main where

import Graphics.Gloss.Interface.Pure.Game hiding (polygon)
import Graphics.Gloss hiding (polygon)
import Debug.Trace
import Mass
import Grid

initialState = State (UI {consPoly = [],
                          consPoint = (0, 0),
                          status = Status {drawing = False}})
                  (World {masses = [],
                          grid = defaultGrid})

main = play (InWindow "pgrav" (900, 900) (0, 0))
              white 100 initialState
              makePicture handleEvent stepWorld

data State = State UI World

type Segment = (Point, Point)

data UI = UI { consPoly :: [Point],
               consPoint :: Point,
               status :: Status }

data World = World { masses :: [MassObject],
                     grid :: Grid}

data Status = Status { drawing :: Bool }

getStatus :: Status -> String
getStatus s = if drawing s then "Drawing" else ""

makePicture :: State -> Picture
makePicture (State ui world)
        = Line (consPoint ui : consPoly ui) `mappend` mconcat (map renderMass (masses world))
          `mappend` Translate (-420) 420 (Scale 0.2 0.2 (Text (getStatus $ status ui)))
          `mappend` gridPicture where
                gridPicture = renderGrid transform (grid world)
                transform (x, y) = (r*sin(theta), (r)*cos(theta)) where
                        r = sqrt(x**2 + y**2)
                        theta = atan2 y x
-- makePicture (State ui world)
--         | UI (Just (a, b)) <- ui
--         , World masses <- world = ,
--                 Pictures $ Line [a, b] : (map Mass.render masses)
--         | World masses <- world = 
--                Pictures $ map Mass.render masses

handleEvent :: Event -> State -> State
handleEvent event state
        | EventMotion pt <- event
        , State ui w <- state
        = State (ui {consPoint = pt}) w

        | EventKey (MouseButton LeftButton) Down _ pt <- event
        , State ui w <- state
        = State (ui {consPoly = (consPoint ui : consPoly ui), consPoint = pt, status = (status ui) {drawing = True}}) w

        | EventKey (SpecialKey KeyEnter) _ _ _ <- event
        , State ui world <- state
        , consPoly ui /= []
        = State (ui { consPoly = [], status = (status ui) {drawing = False} })  (world {masses = (newPoly (consPoly ui) : masses world)})
        
        | otherwise
        , State ui (World {masses = masses, grid = grid}) <- state
        = State ui (World {masses = foldl (++) [] (map (\m -> respond m event) masses), grid = grid})

stepWorld :: Float -> State -> State
stepWorld _ (State ui (World {masses = masses, grid = grid})) = State ui (World {masses = map updateMass masses, grid = grid})
