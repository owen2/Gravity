# Gravity

This is a Gravity simulator written in Haskell, using the [Gloss library](https://hackage.haskell.org/package/gloss) for graphics.
